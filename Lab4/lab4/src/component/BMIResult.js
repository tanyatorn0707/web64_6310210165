function BMIResult(promps) {
    return(

        <div>
            <h2>คุณ: {promps.name } </h2>
            <h3>มี BMI:{promps.bmi }</h3>
            <h4>แปลว่า: {promps.result }</h4>
        </div>
    );
}

export default BMIResult;