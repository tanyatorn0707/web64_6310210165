function AboutUs(props) {


    return(
        <div>
            <h2>จัดทำโดย: { props.name } </h2>
            <h3>ติดต่อธัญธร ได้ที่{ props.address }</h3>
            <h4>บ้านธัญธร อยู่ที่ { props.province }</h4>
        </div>    

    );
}
export default AboutUs;
