import logo from './logo.svg';
import './App.css';

import AboutUsPage from './page/AboutUsPage';
import BMICalPage from './page/BMICalPage';
import Header from './component/Header';
import LuckyNumberPage from './page/LuckyNumberPage';
import { Routes, Route, Link } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
          <Route path ="about" element= {
            <AboutUsPage />
          } />

          <Route path ="/" element= {
            <BMICalPage />
          } />

          <Route path ="luckynumber" element= {
            <LuckyNumberPage />
          } />
          
      </Routes>
    </div>
  );
}

export default App;