import BMIResult from "../component/BMIResult";
import {useState} from "react" ;
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid' ;
import { Typography,Box,Container } from "@mui/material";
function BMICalPage() {
    const [name,setName] = useState("") ;
    const [bmiResult,setBmiResult] = useState(0) ;
    const [translateResult,setTranslateResult] = useState("") ;
    const [height,setHeight] = useState("") ;
    const [weight,setWeight] = useState("") ;
    function calculateBMI(){
        let h = parseFloat(height) ;
        let w = parseFloat(weight) ;
        let bmi = w/(h*h) ;
        setBmiResult(bmi) ;
        if (bmi > 25){
            setTranslateResult("อ้วน");
        }else{
            setTranslateResult("ผอม");
        }
    }
return(
    <Container maxWidth = 'lg'>
    <Grid container spacing={2} sx = {{margin : "10px"}}>
  <Grid item xs={12}>
    <Typography variant = "h5">
        ยินดีต้อนรับสู่เว็บคำนวณ BMI
    </Typography>
  </Grid>
  <Grid item xs={8} >
       <Box sx = {{textAlign : 'left'}}>
            คุณชื่อ : <input type = "text" 
                          value = {name}
                          onChange = { (e) => { setName(e.target.value) ;} }/> 
                          <br />
                          <br />
            ส่วนสูง : <input type = "text" 
                          value = {height}
                          onChange = { (e) => { setHeight(e.target.value) ;} }/> 
                          <br />
                          <br />
            น้ำหนัก : <input type = "text" 
                          value = {weight}
                          onChange = { (e) => { setWeight(e.target.value) ;} }/>  
                          <br />
                          <br />

<Button variant="contained" onClick={() => {calculateBMI()}} >
    Calculate
    </Button>
    </Box>
  </Grid>
  <Grid item xs={4}>
  { bmiResult != 0 &&
<div>
<hr />
นี่คือผลลัพธ์การคำนวณ
<BMIResult 
name = {name}
bmi  = {bmiResult}
result = {translateResult}
/>
</div>
  }
  </Grid>
</Grid>
</Container>


);
}
export default BMICalPage;

/*<div align = "left">
    <div align = "center">
ยินดีต้อนรับสู่เว็บคำนวณ BMI 
    <hr />
คุณชื่อ : <input type = "text" 
               value = {name}
               onChange = { (e) => { setName(e.target.value) ;} }/> <br />
ส่วนสูง : <input type = "text" 
value = {height}
onChange = { (e) => { setHeight(e.target.value) ;} }/> <br />
น้ำหนัก : <input type = "text" 
value = {weight}
onChange = { (e) => { setWeight(e.target.value) ;} }/> <br />

<Button variant="contained" onClick={() => {calculateBMI()}} >
    Calculate
    </Button>;

{ bmiResult != 0 &&
<div>
<hr />
นี่คือผลลัพธ์การคำนวณ
<BMIResult 
name = {name}
bmi  = {bmiResult}
result = {translateResult}
/>
</div>
}
</div>
</div>
*/