const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/score', (req, res) => {
    let value = parseFloat(req.query.value)
    var Grade = {}

    if ((value >= 80) && (value <= 100)) {
            Grade = "A"
         
        }else if ((value >= 75) && (value <= 79)) {
                Grade = "B+"
            }
        
        else if ((value >= 70) && (value <= 74)) {
            Grade = "B"
        }
        else if ((value >= 65) && (value <= 69)) {
        Grade = "C+"
    }
else if ((value >= 60) && (value <= 64)) {
        Grade = "C"
    }
else if ((value >= 55) && (value <= 59)) {
        Grade = "D+"
    }
else if ((value >= 50) && (value <= 54)) {
        Grade = "D"
    }
else if ((value >= 0) && (value <= 49)) {
        Grade = "E"
        }else {
            grade = {
                "Message" : "Score is not number"
            }
            }
        
    res.send(JSON.stringify('Mr./Mrs. ' + req.query.name + ',' + 'Your grade = ' + Grade))
  })

app.get('/triangle', (req, res) => {
    let base = parseFloat(req.query.base)
    let height = parseFloat(req.query.height)
    var result = {}

    if (!isNaN(base)&& !isNaN(height)) {

    let area = 0.5 * height * base

     result = {
        "status" : 200,
        "area" : area 
     }
    }else {

        result = {
            "status" : 400 ,
            "message" : "Base or Height is not number"
        }
    }
    res.send(JSON.stringify(result))
  })

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})